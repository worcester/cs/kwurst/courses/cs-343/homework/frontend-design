# Homework 6 &mdash; Frontend Design and Implementation

## Fork Frontend

**Fork the Frontend (`https://gitlab.com/LibreFoodPantry/training/microservices/microservices-examples/frontend`) to your private GitLab subgroup of the `CS 343 01 02 E1 Fall 2021 / Students` group and clone it to your computer.**

**You will be working in the `main` branch.**

## *Base Assignment*

**Everyone must complete this portion of the assignment in a *Meets Specification* fashion.**

### Modify frontend to filter items by name

1. Add an input field. Connect it to the `v-model`.
2. Add a button `Filter by name`. Connect it to a `filterByName` function.
3. Write your `filterByName` method to get the items from the backend, using the appropriate endpoint.
4. Test your new frontend changes.

#### Once you are happy with your work, commit your changes with the message `Base Assignment`.

## *Intermediate Add-On*

**Complete this portion in a *Meets Specification* fashion to apply towards base course grades of C or higher**

* See the the Course Grade Determination table in the syllabus to see how many Intermediate Add-Ons you need to submit for a particular letter grade.
* See My Grades in Blackboard to see how many you have submitted and received an Acceptable grade on.
* You may not need to submit one for this assignment.

### Modify frontend to allow the name of an item to be modified

1. Add an input field for `_id`. Label it in the HTML. Connect it to the `v-model`.
2. Add an input field for `name`. Label it in the HTML. Connect it to the `v-model`.
3. Add a button `Update Item name`. Connect it to an `updateItem` function.
3. Write your `updateItem` method to update the item in the backend, using the appropriate endpoint.
4. Test your new frontend changes.

#### Once you are happy with your work, commit your changes with the message `Intermediate Assignment`.

## *Advanced Add-On*

**Complete this portion in a *Meets Specification* fashion to apply towards base course grades of B or higher**

* See the the Course Grade Determination table in the syllabus to see how many Intermediate Add-Ons you need to submit for a particular letter grade.
* See My Grades in Blackboard to see how many you have submitted and received an Acceptable grade on.
* You may not need to submit one for this assignment.

### Create frontend design to allow items in an order to be modified

For this part of the assignment, you will only be designing the frontend changes, not implementing them.

Design a frontend component that will:

1. Let the user enter an order id.
2. Display the order with id, email, preferences, restrictions, and a list of all items in the order.
    * The email, preferences, and restrictions should be shown in input fields.
    * The user can modify email, preferences, and/or restrictions.
    * The user cannot modify id.
    * The list of items should have a checkbox next to each item.
    * If the item is already in the order, the checkbox should be checked. If not, the checkbox should be unchecked.
    * The user can check and uncheck any check boxes.
3. Add a button `Update order`.
4. Draw a picture of what this will look like. Label the parts of the picture.
5. Write "pseudo-HTML" for this new frontend. You should already know all all the tags needed, except for the checkboxes. You can make up a tag for the checkboxes, or you can look up the correct tag. You do not need to get everything completely correct, but you should be able to get the non-checkbox parts correct.
6. Write pseudocode for the method that will be called by the `Update order` button. You can do this with the already-defined endpoints, but it may be easier to do with a new `PUT /orders/id` endpoint that takes the entire order as a request body. If you decide to do it this way, write a "pseudo-OpenAPI" definition for the modified definition. (Just explain how it is different from the one that is already defined.) 

Include a file for your diagram for part 4 and a text file for your pseudo-* in parts 5 and 6.

#### Once you are happy with your work, commit your changes with the message `Advanced Assignment`.

## Deliverables

* You do not need to fork `HW6` to your private subgroup. Your submission for HW6 will be your fork of `frontend` in your private subgroup.
* You must push your commit(s) to your project.

## Due Date

Monday, 13 December 2021 - 11:59pm

&copy; 2021 Karl R. Wurst.

<img src="http://mirrors.creativecommons.org/presskit/buttons/88x31/png/by-sa.png" width=100px/>This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License. To view a copy of this license, visit <a href="http://creativecommons.org/licenses/by-sa/4.0/" target="_blank">http://creativecommons.org/licenses/by-sa/4.0/</a> or send a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.
